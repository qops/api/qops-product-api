########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

import logging

# Import models
from .models import Product
from .models import ProductsDashboard

#from .models import ProductTags
from .models import ProductComponents
from .models import ProductCost
from ..mod_issue.models import ProductIssues
from ..mod_issue.models import Issue

from .forms import ProductProfileForm
from .forms import ProductComponentForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_product = Blueprint('product', __name__, url_prefix='/product')


@mod_product.context_processor
def store():
    store_dict = {'serviceName': 'Product',
                  'serviceDashboardUrl': url_for('product.dashboard'),
                  'serviceBrowseUrl': url_for('product.browse'),
                  'serviceNewUrl': url_for('product.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_product.route('/', methods=['GET'])
def product():
    return render_template('product/product_dashboard.html')


@mod_product.route('/dashboard', methods=['GET'])
def dashboard():
    products_dashboard_data = ProductsDashboard()
    
    return render_template('product/product_dashboard.html',
                           dashboard_data=products_dashboard_data)


@mod_product.route('/browse', methods=['GET'])
def browse():
    products = Product.query.all()
    return render_template('product/product_browse.html', products=products)


@mod_product.route('/new', methods=['GET', 'POST'])
def new():
    product = Product()
    form = ProductProfileForm(request.form)
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN PRODUCT.NEW(POST)')
        logger.debug('Product Identifier        : {0}'.format(
            form.identifier.data))
        logger.debug('Product Name              : {0}'.format(form.name.data))
        logger.debug('Product Description       : {0}'.format(
            form.description.data))
        logger.debug('Product Internal Code Name: {0}'.format(
            form.internal_code_name.data))
        logger.debug('Product External Code Name: {0}'.format(
            form.external_code_name.data))
        form.populate_obj(product)
        product.identifier = Product.get_next_identifier()
        db.session.add(product)
        db.session.commit()
        return redirect(url_for('product.browse'))
    return render_template('product/product_new.html', product=product, form=form)


@mod_product.route('/profile', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/profile', methods=['GET', 'POST'])
def profile(product_id=None):
    product = Product.query.get(product_id)
    form = ProductProfileForm(obj=product)
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN PRODUCT.PROFILE(POST)')
        logger.debug('Product Identifier        : {0}'.format(
            form.identifier.data))
        logger.debug('Product Name              : {0}'.format(form.name.data))
        logger.debug('Product Description       : {0}'.format(
            form.description.data))
        logger.debug('Product Internal Code Name: {0}'.format(
            form.internal_code_name.data))
        logger.debug('Product External Code Name: {0}'.format(
            form.external_code_name.data))
        form = ProductProfileForm(request.form)
        form.populate_obj(product)
        db.session.add(product)
        db.session.commit()
        return redirect(url_for('product.browse'))
    return render_template('product/product_profile.html', product=product, form=form)


@mod_product.route('/view', methods=['GET', 'POST'])
@mod_product.route('/view/<int:product_id>/view', methods=['GET',
                                                           'POST'])
def product_view(product_id=None):
    #product = Product.query.get(product_id)
    form = ProductProfileForm(obj=product)
    if request.method == 'POST':
        form = ProductProfileForm(request.form)
        form.populate_obj(product)
        db.session.add(product)
        db.session.commit()
        return redirect(url_for('product.browse'))
    return render_template('product/product_view.html', product=product,
                           form=form)


@mod_product.route('/profile/dashboard', methods=['GET'])
@mod_product.route('/profile/<int:product_id>/dashboard', methods=['GET'])
def product_dashboard(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_dashboard.html', product=product)


@mod_product.route('/profile/backlog', methods=['GET'])
@mod_product.route('/profile/<int:product_id>/backlog', methods=['GET'])
def product_backlog(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_backlog.html', product=product)


@mod_product.route('/profile/communication', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/communication', methods=['GET'])
def product_communication(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_communication.html', product=product)


@mod_product.route('/profile/releases', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/releases', methods=['GET'])
def product_releases(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_releases.html', product=product)


@mod_product.route('/profile/builds', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/builds', methods=['GET'])
def product_builds(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_builds.html', product=product)


@mod_product.route('/profile/requirements', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/requirements', methods=['GET'])
def product_requirements(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_requirements.html', product=product)


@mod_product.route('/profile/documents', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/documents', methods=['GET'])
def product_documents(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_documents.html', product=product)


@mod_product.route('/profile/customers', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/customers', methods=['GET'])
def product_customers(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_customers.html', product=product)


@mod_product.route('/profile/locations', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/locations', methods=['GET'])
def product_locations(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_locations.html', product=product)


@mod_product.route('/profile/issues', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/issues', methods=['GET'])
def product_issues(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    issues = Issue.query.with_entities(Issue.id, Issue.identifier, Issue.title).join(
        ProductIssues, Issue.id == ProductIssues.issue_id).filter_by(product_id=product_id).all()
    return render_template('product/product_single_issues.html', product=product, issues=issues)


@mod_product.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/test-cases', methods=['GET'])
def product_test_cases(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
    else:
        product = None
    return render_template('product/product_single_test_cases.html', product=product)


@mod_product.route('/profile/components', methods=['GET', 'POST'])
@mod_product.route('/profile/<int:product_id>/components', methods=['GET', 'POST'])
def product_components(product_id=None):
    if product_id:
        product = Product.query.get(product_id)
        product_components = ProductComponents.query.filter_by(product_id = product_id)
    else:
        product = None
        product_components = None
    form = ProductComponentForm()
    return render_template('product/product_single_components.html', product=product, product_components=product_components, form=form)


def test1(b, y):
    issue = Issue.query.get(b)
    product = product.query.get(y)
    product.issues.append(issue)
    db.session.add(product)
    db.session.commit()
    return 'Issue ID is {0} and Product ID is {1}.'.format(b, y)


@mod_product.route('/snc/', methods=['POST'])
def snc():
    comp = ProductComponents()
    comp.product_id = request.json['product_id']
    comp.name = request.json['name']
    comp.usage = request.json['usage']
    db.session.add(comp)
    db.session.commit()
    return 'does it work'
