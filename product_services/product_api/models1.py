########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref
from marshmallow import Schema, fields, ValidationError, pre_load


from ..mod_tag.models import Tag

from qops_desktop import db
from qops_desktop import ma

class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime,
                           default=db.func.now(),
                           onupdate=db.func.now())


# ProductTags = db.Table('product_tags',
#                       db.Column('product_id', db.Integer, db.ForeignKey('product.id'), primary_key=True),
#                       db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'), primary_key=True),
#                       info={'bind_key': 'qops'})


class Product(Base):
    __tablename__ = 'product'

    identifier = db.Column(db.String)
    name = db.Column(db.String)
    internal_code_name = db.Column(db.String)
    external_code_name = db.Column(db.String)
    description = db.Column(db.String)
#   tags = db.relationship('Tag', secondary=ProductTags, backref='products')
    def get_next_identifier():
        return 'PRD00001'


class ProductCost(Base):
    __tablename__ = 'product_cost'

    product_id = db.Column(db.Integer)
    


class ProductComponents(Base):
    __tablename__ = 'product_components'

    identifier = db.Column(db.String(25))
    product_id = db.Column(db.Integer)
    name = db.Column(db.String)
    usage = db.Column(db.String)
    parent_id = db.Column(db.Integer)


class ProductComponentsSchema(Schema):
    product_id = fields.Int(dump_only=True)
    name = fields.Str()
    usage = fields.Str()
    parent_id = fields.Int()


class ProductsDashboard():
    
    def __init__(self):
        self.last_product_created = self.last_product_created()


    def last_product_created(self):
        return Product.query.order_by('-id').first()


class ProductComponent():
    """

    """

    def __init__(self, name, usage, parent_id):
        self._name = name,
        self._usage = usage,
        self._parent_id = parent_id

    def exists(self, id):
        components = ProductComponents.query.all()

        if id in components:
            return True;
        else:
            return False;

    #insert(self):
        
